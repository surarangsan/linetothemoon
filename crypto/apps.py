from django.apps import AppConfig


class CryptocalConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'crypto'

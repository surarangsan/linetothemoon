from django.core.paginator import Paginator
from django.shortcuts import render
from matplotlib.font_manager import json_load
from crypto.models import *
from datetime import datetime, timedelta
from django.http import HttpResponse, JsonResponse
import pytz
from django.views.decorators.csrf import csrf_exempt
from crypto.views.Helpers import Helper


import logging
logger = logging.getLogger('django')

class Users():

    def Register(request):
        return render(request, 'users/register.html')

    @csrf_exempt
    def CreateUser(request):
        lineId = request.POST.get('line_id', '')
        username = request.POST.get('username', '')
        password = request.POST.get('password', '')
        dataUser = users.objects.all().filter(line_id=lineId).values()
        if(len(dataUser) == 0):
            userInsert = users()
            userInsert.line_id = lineId
            userInsert.username = username
            userInsert.password = password
            userInsert.crypto = '(btc),'
            userInsert.daily_status = 1
            userInsert.is_login = 0
            userInsert.noti_status = 1
            userInsert.status = 1
            userInsert.date = datetime.now(pytz.timezone('Asia/Bangkok')).strftime('%Y-%m-%d %H:%M:%S')
            userInsert.save()
            return HttpResponse(status=200)
        else:
            return HttpResponse(status=500)
            
    def Login(request):
        return render(request, 'users/login.html')

    @csrf_exempt
    def IsLogin(request):
        lineId = request.POST.get('line_id')
        user = users.objects.all().filter(line_id=lineId).values()
        if(user[0]['is_login'] == 1):
            request.session['data'] = lineId
            update = users.objects.get(line_id=lineId)
            update.is_login = 1
            update.save()
            return HttpResponse(status=200)
        else:
            return HttpResponse(status=403)
      
    @csrf_exempt
    def LogOut(request):
        if 'data' not in request.session:
            return HttpResponse(status=403)
        lineId = request.session['data']
        update = users.objects.get(line_id=lineId)
        update.is_login = 0
        update.save()
        request.session.pop('data')
        return HttpResponse(status=200)

    def CheckLogin(request):
        usernameLogin = request.POST.get('username')
        passwordLogin = request.POST.get('password')
        user = users.objects.all().filter(username=usernameLogin).values()
        if(len(user) != 0):
            if(user[0]['username'] == usernameLogin and user[0]['password'] == passwordLogin):
                lineId = user[0]['line_id']
                request.session['data'] = lineId
                update = users.objects.get(line_id=lineId)
                update.is_login = 1
                update.save()

                return HttpResponse(status=200)
            else:
                return HttpResponse(status=403)
        else:
            return HttpResponse(status=403)

    def Homes(request):
        if 'data' not in request.session:
            return HttpResponse(status=403)
        lineId = request.session['data']
        dataUser = users.objects.all().filter(line_id=lineId).values()
        cryptoArr = dataUser[0]['crypto'].replace('(','').replace(')','')[:-1]
        cryptoArr = cryptoArr.split(',')
        if(dataUser[0]['crypto']):
            arraysCrypto = []
            dateTimeArrays = []
            predict = 4
            cryptoString = ""
            for itemCrypto in cryptoArr:
                cryptoString = cryptoString  + "(" + itemCrypto + ") "
            for itemCrypto in cryptoArr:
                subArraysCrypto = []
                predictArrays = []
                volumeArrays = []
                currencyPriceArr = []
                currencyVolumeArr = []
                subArraysCrypto.append(itemCrypto)
                predictPrice = Helper.GetPriceToPredictFromDb(itemCrypto, predict, "price_usd")
                predictVolume = Helper.GetPriceToPredictFromDb(itemCrypto, predict, "volume")
                cryptoData = cryptocurrency_value_fifteen.objects.all().filter(crypto=str(itemCrypto).lower()).order_by('-id').values()
                currencyPriceArr.append(Helper.FormatPrice(cryptoData[0]['price_usd']))
                currencyVolumeArr.append(Helper.FormatNumber(cryptoData[0]['volume']))

                predictArrays.append(cryptoData[1]['price_usd'])
                volumeArrays.append(cryptoData[1]['volume'])
                predictArrays.append(cryptoData[0]['price_usd'])
                volumeArrays.append(cryptoData[0]['volume'])
                
                for itemPrice in predictPrice:
                    predictArrays.append(itemPrice) 
                for itemVolume in predictVolume:
                    volumeArrays.append(itemVolume) 

                subArraysCrypto.append(currencyPriceArr)
                subArraysCrypto.append(currencyVolumeArr)
                subArraysCrypto.append(predictArrays)
                subArraysCrypto.append(volumeArrays)
                arraysCrypto.append(subArraysCrypto)

                

            dateTime = cryptoData[0]['date_time']
            latestUpdate = (dateTime).strftime('%d-%m-%Y %H:%M:%S')
            nextUpdate = (dateTime) + timedelta(minutes=15)
            nextUpdate = nextUpdate.strftime('%d-%m-%Y %H:%M:%S')

            nextRefresh = (dateTime) + timedelta(minutes=15)
            nextRefresh = nextRefresh.strftime('%Y-%m-%d %H:%M:%S')

            dateTime = cryptoData[0]['date_time'] - timedelta(minutes=15)
            dateTimeArrays.append(dateTime.strftime('%H:%M:%S'))

            dateTime = cryptoData[0]['date_time']
            dateTimeArrays.append(dateTime.strftime('%H:%M:%S'))

            for day in range(predict):
                dateTimeArrays.append((dateTime).strftime('%H:%M:%S'))
                dateTime = dateTime + timedelta(minutes=15)

            dateTime = datetime.now(pytz.timezone('Asia/Bangkok')).strftime('%d-%m-%Y')
            title = ["DashBoard", "แสดงเหรียญทั้งหมดที่มีในระบบ", cryptoString,latestUpdate, nextUpdate, dateTime]
        else: 
            arraysCrypto = []
            dateTimeArrays = []
            title = []
            nextRefresh = ""
        return render(request,'users/home.html',{'crypto': arraysCrypto,'dateTimeArrays' : dateTimeArrays, 'title' : title, 'nextRefresh' : nextRefresh, 'line_id': lineId})
    
    def Home(request):
        if 'data' not in request.session:
            return HttpResponse(status=403)
        lineId = request.session['data']
        dataUser = users.objects.all().filter(line_id=lineId).values()
        cryptoArr = dataUser[0]['crypto'].replace('(','').replace(')','')[:-1]
        cryptoArr = cryptoArr.split(',')

        arraysCrypto = []
        limit = 10
        predict = 4
        cryptoString = ""
        for itemCrypto in cryptoArr:
            cryptoString = cryptoString  + "(" + itemCrypto + ") "

        for itemCrypto in cryptoArr:
            prediction = Helper.GetPriceToPredictFromDbGraph(itemCrypto, predict, limit)
            arraysCrypto.append(prediction)

        lastest = cryptocurrency_value_fifteen.objects.all().order_by('-id').values()
        lastUpdate = lastest[0]['date_time'].strftime('%H:%M:%S')
        nextUpdate = (lastest[0]['date_time'] + timedelta(minutes=15)).strftime('%H:%M:%S')
        nextRefresh = (lastest[0]['date_time'] + timedelta(minutes=15)).strftime('%Y-%m-%d %H:%M:%S')

        return render(request, 'users/home.html', {'arraysCrypto' : arraysCrypto, 'cryptoString' : cryptoString, 'lastUpdate' : lastUpdate, 'nextUpdate' : nextUpdate, 'nextRefresh' : nextRefresh})

    def Crypto(request):
        if 'data' not in request.session:
            return HttpResponse(status=403)
        lineId = request.session['data']
        crypto  = request.GET.get('crypto')
        dataUser = users.objects.all().filter(line_id=lineId).values()
        cryptoArr = dataUser[0]['crypto'].replace('(','').replace(')','')[:-1]
        cryptoArr = cryptoArr.split(',')

        arraysCrypto = []
        limit = 10
        predict = 4
        cryptoString = ""
        for itemCrypto in cryptoArr:
            cryptoString = cryptoString  + "(" + itemCrypto + ") "

        prediction = Helper.GetPriceToPredictFromDbGraph(crypto, predict, limit)
        arraysCrypto.append(prediction)

        lastest = cryptocurrency_value_fifteen.objects.all().order_by('-id').values()
        lastUpdate = lastest[0]['date_time'].strftime('%H:%M:%S')
        nextUpdate = (lastest[0]['date_time'] + timedelta(minutes=15)).strftime('%H:%M:%S')
        nextRefresh = (lastest[0]['date_time'] + timedelta(minutes=15)).strftime('%Y-%m-%d %H:%M:%S')

        detail = cryptocurrency_profile.objects.all().filter(crypto=crypto).values()

        profile = {
            'name' : detail[0]['name'],
            'details' : detail[0]['details'],
             'details_technology' : detail[0]['technology_details'],
              'details_background' : detail[0]['background_details'],
              'individuals' : detail[0]['individuals'],
              'organizations' : detail[0]['organizations'],
              'allocated_to_organization_or_founders_percentage' : detail[0]['allocated_to_organization_or_founders_percentage'],
              'allocated_to_investors_percentage' : detail[0]['allocated_to_investors_percentage'],
              'allocated_to_premined_rewards_or_airdrops_percentage' : detail[0]['allocated_to_premined_rewards_or_airdrops_percentage']
        }


        return render(request, 'users/crypto.html', {'arraysCrypto' : arraysCrypto, 'cryptoString' : cryptoString, 'lastUpdate' : lastUpdate, 'nextUpdate' : nextUpdate, 'profile' : profile, 'nextRefresh' : nextRefresh})
 
    def Profile(request):
        if 'data' not in request.session:
            return HttpResponse(status=403)

        lineId = request.session['data']
        userData = users.objects.all().filter(line_id=lineId).values()
        cryptoString = ""
        
        if(userData[0]['crypto']):
            cryptoArr = userData[0]['crypto'].replace('(','').replace(')','')[:-1]
            cryptoArr = cryptoArr.split(',')

            for itemCrypto in cryptoArr:
                cryptoString = cryptoString  + "(" + itemCrypto + ") "
        else:
            cryptoArr =['ไม่มี']
        crypto = []
        for itemCrypto in cryptoArr:
            crypto.append(itemCrypto.upper())
        user = {
            'username' : userData[0]['username'],
            'crypto' : crypto,
            'date' : userData[0]['date'].strftime('%d-%m-%Y %H:%M:%S'),
            'noti_status' : userData[0]['noti_status'],
            'daily_status' : userData[0]['daily_status'],
        }
        return render(request, 'users/profile.html', {'user':user, 'cryptoString' : cryptoString})

    @csrf_exempt
    def ChangeNotiStatus(request):
        if 'data' not in request.session:
            return HttpResponse(status=403)
        type  = request.POST.get('type')
        lineId = request.session['data']
        user = users.objects.all().filter(line_id=lineId).values()
        status = 0
        if(user[0][type] == 0 ):
            status = 1
        else:
            status = 0 
        update = users.objects.get(line_id=lineId)
        if(type == 'noti_status'):
            update.noti_status = status
        else:
            update.daily_status = status
        
        update.save()

        return HttpResponse(status=200)

    def Coin(request):
        if 'data' not in request.session:
            return HttpResponse(status=403)
        lineId = request.session['data']
        user = users.objects.all().filter(line_id=lineId).values()
        cryptoString = ""
        
        if(user[0]['crypto']):
            cryptoArr = user[0]['crypto'].replace('(','').replace(')','')[:-1]
            cryptoArr = cryptoArr.split(',')

            for itemCrypto in cryptoArr:
                cryptoString = cryptoString  + "(" + itemCrypto + ") "
    
            userCrypto = []
            for itemCrypto in cryptoArr:
                userCrypto.append(itemCrypto)
        
            userCryptoLen = len(userCrypto)

            system = cryptocurrency.objects.all().values()
            systrmCrypto = []
            for itemCrypto in system:
                systrmCrypto.append(itemCrypto['name_short'])

            notAdd = []
            for itemCrypto in systrmCrypto:
                if itemCrypto not in userCrypto:
                    notAdd.append(itemCrypto)

        else:
            system = cryptocurrency.objects.all().values()
            systrmCrypto = []
            for itemCrypto in system:
                systrmCrypto.append(itemCrypto['name_short'])

            notAdd = systrmCrypto

            userCrypto = []

            userCryptoLen = len(userCrypto)

        return render(request, 'users/coin.html', {'userCrypto':userCrypto, 'systrmCrypto':systrmCrypto, 'notAdd':notAdd, 'userCryptoLen':userCryptoLen, 'cryptoString':cryptoString})

    @csrf_exempt
    def AddCoin(request):
        if 'data' not in request.session:
            return HttpResponse(status=403)
        coin  = request.POST.get('coin')
        lineId = request.session['data']
        user = users.objects.all().filter(line_id=lineId).values()
        cryptoUser = user[0]['crypto']
        cryptoUser = cryptoUser + "("+coin+"),"
        update = users.objects.get(line_id=lineId)
        update.crypto = cryptoUser
        update.save()

        return HttpResponse(status=200)

    @csrf_exempt
    def RemoveCoin(request):
        if 'data' not in request.session:
            return HttpResponse(status=403)
        coin  = request.POST.get('coin')
        lineId = request.session['data']
        user = users.objects.all().filter(line_id=lineId).values()
        cryptoUser = user[0]['crypto']

        if "("+coin+")," in cryptoUser:
            cryptoUser = cryptoUser.replace("("+coin+"),","")
            update = users.objects.get(line_id=lineId)
            update.crypto = cryptoUser
            update.save()
            return HttpResponse(status=200)
        else:
            return HttpResponse(status=500)

    def Question(request):
        if 'data' not in request.session:
            return HttpResponse(status=403)
        lineId = request.session['data']
        question = questions.objects.all().filter(line_id=lineId).values()
        user = users.objects.all().filter(line_id=lineId).values()
        cryptoString = ""
        
        if(user[0]['crypto']):
            cryptoArr = user[0]['crypto'].replace('(','').replace(')','')[:-1]
            cryptoArr = cryptoArr.split(',')

            for itemCrypto in cryptoArr:
                cryptoString = cryptoString  + "(" + itemCrypto + ") "

        return render(request, 'users/questions.html' , {'question':question,'cryptoString':cryptoString})

    @csrf_exempt
    def RemoveQuestions(request):
        if 'data' not in request.session:
            return HttpResponse(status=403)
        id = request.POST.get('id', '')
        data = questions.objects.all().filter(id=id).values()
        if(len(data) != 0):
            questions.objects.filter(id=id).delete()
            return HttpResponse(status=200)
        else:
            return HttpResponse(status=500)

    @csrf_exempt
    def SendQuestions(request):
        if 'data' not in request.session:
            return HttpResponse(status=403)
        lineId = request.session['data']
        question = request.POST.get('question', '')
        user = users.objects.all().filter(line_id=lineId).values()
        update = questions()
        update.status = 0
        update.question = question
        update.line_id = lineId
        update.username = user[0]['username']
        update.date_time = datetime.now(pytz.timezone('Asia/Bangkok')).strftime('%Y-%m-%d %H:%M:%S')
        update.save()
        Helper.SendLineNoti('Ufe56503cd2d0b7b0cb0fe4a68a8a11ea', 'มีข้อความส่งถึงคุณ')
        return HttpResponse(status=200)
        
    def News(request):
        if 'data' not in request.session:
            return HttpResponse(status=403)
        lineId = request.session['data']

        data = news.objects.all().order_by('-date_time').values()

        dataPaginator = Paginator(data, 5)

        page_num = request.GET.get('page')

        page = dataPaginator.get_page(page_num)

        user = users.objects.all().filter(line_id=lineId).values()

        cryptoString = ""

        if(user[0]['crypto']):
            cryptoArr = user[0]['crypto'].replace('(','').replace(')','')[:-1]
            cryptoArr = cryptoArr.split(',')

            for itemCrypto in cryptoArr:
                cryptoString = cryptoString  + "(" + itemCrypto + ") "

        return render(request, 'users/news.html',{'page':page,'cryptoString':cryptoString})

    def NewsDetail(request):
        if 'data' not in request.session:
            return HttpResponse(status=403)
        lineId = request.session['data']
        id = request.GET.get('id')
        data = news.objects.all().filter(id=id).values()
        user = users.objects.all().filter(line_id=lineId).values()
        if(user[0]['crypto']):
            cryptoArr = user[0]['crypto'].replace('(','').replace(')','')[:-1]
            cryptoArr = cryptoArr.split(',')
            cryptoString = ""
            for itemCrypto in cryptoArr:
                cryptoString = cryptoString  + "(" + itemCrypto + ") "
        return render(request, 'users/newsDetail.html',{'data':data[0],'cryptoString':cryptoString})
        
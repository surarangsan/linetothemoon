"""linetothemoon URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from crypto.views import views
from crypto.views.Helpers import Helper
from crypto.views.Admin import Admin
from crypto.views.Task import Task
from crypto.views.Users import Users
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('login/', Admin.Login),

    path('home/', Admin.Home),

    path('news/', Admin.News),

    path('crypto/', Admin.Crypto),

    path('deleteCrypto/', Admin.DaleteCrypto),

    path('deleteNews/', Admin.DaleteNews),

    # path('sendNews/', Admin.SendNews),

    path('newsDetail/', Admin.NewsDetail),

    path('editCrypto/', Admin.EditCrypto),

    path('createCrypto/', Admin.CreateCrypto),

    path('users/', Admin.Users),

    path('deleteUsers/', Admin.DaleteUsers),

    path('editUsers/', Admin.EditUsers),

    path('createUsers/', Admin.CreateUsers),

    path('questions/', Admin.Questions),

    path('deleteQuestion/', Admin.DeleteQuestions),

    path('sendQuestions/', Admin.SendQuestions),

    path('checklogin/', Admin.CheckLogin),

    path('users/islogin/', Users.IsLogin),

    path('users/login/', Users.Login),

    path('users/logout/', Users.LogOut),

    path('users/register/', Users.Register),

    path('users/createUser/', Users.CreateUser),

    path('users/checklogin/', Users.CheckLogin),

    path('users/home/', Users.Home),

    path('users/crypto/', Users.Crypto),

    path('users/profile/', Users.Profile),

    path('users/changenotistatus/', Users.ChangeNotiStatus),

    path('users/coin/', Users.Coin),

    path('users/addcoin/', Users.AddCoin),

    path('users/removecoin/', Users.RemoveCoin),

    path('users/questions/', Users.Question),

    path('users/deletequestions/', Users.RemoveQuestions),

    path('users/sendquestions/', Users.SendQuestions),

    path('users/news/', Users.News),

    path('users/newsdetail/', Users.NewsDetail),

    # http://127.0.0.1:8000/graphdaily/
    path('graphdaily/', views.GraphDaily),

    # http://127.0.0.1:8000/dashboard/
    path('dashboard/', views.DashBoard),

    # http://127.0.0.1:8000/dashboard/detail/
    path('dashboard/detail/', views.DashBoardDetail),

    # http://127.0.0.1:8000/help/
    path('help/', views.Help),

    # http://127.0.0.1:8000/news
    path('getnews/', Task.NewsApi),

    # http://127.0.0.1:8000/getcryptocurrencyfifteenvalue
    path('getcryptocurrencyfifteenvalue/', Task.MarketCapFifteenApi),

    # http://127.0.0.1:8000/getcryptocurrencyonevalue
    path('getcryptocurrencyonevalue/', Task.MaketCapOneApi),

    # http://127.0.0.1:8000/getcryptocurrencyprofile
    path('getcryptocurrencyprofile/', Task.CryptoProfileApi),

    # http://127.0.0.1:8000/cryptonotipriceapi
    path('cryptonotipriceapi/', Task.CryptoNotiPriceApi),

    # http://127.0.0.1:8000/cryptonotipricedailyapi
    path('cryptonotipricedailyapi/', Task.CryptoNotiPriceDailyApi),

    # http://127.0.0.1:8000/cryptorankapi
    path('cryptorankapi/', Task.CryptoRankApi),

    # http://127.0.0.1:8000/webhook/
    path('webhook/', views.WebHook),
    
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
from datetime import datetime
from django.db import models

def get_img_upload_path(instance, filename):
    return f'{instance.name}/images/{filename}'

class cryptocurrency_value_fifteen(models.Model):
    id = models.AutoField(primary_key=True)
    crypto = models.TextField()
    price_usd = models.FloatField()
    volume = models.FloatField()
    price_usd_percentage = models.FloatField()
    volume_percentage = models.FloatField()
    date_time = models.DateTimeField()
    market_cap = models.FloatField()
    status = models.IntegerField()

class cryptocurrency_value_daily(models.Model):
    id = models.AutoField(primary_key=True)
    crypto = models.TextField()
    grow_rate = models.FloatField()
    close = models.FloatField()
    open = models.FloatField()
    date_time = models.DateTimeField()
    status = models.IntegerField()

class news(models.Model):
    id = models.AutoField(primary_key=True)
    type  = models.BigIntegerField()
    topic  = models.TextField()
    detail = models.TextField()
    cryptocurrency_id = models.IntegerField()
    date_time  = models.DateTimeField()
    source = models.TextField()
    image = models.TextField()
    link = models.TextField()
    status = models.IntegerField()

class cryptocurrency(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.TextField()
    image = models.TextField()
    name_short = models.TextField()
    start_date = models.DateTimeField()
    status = models.BigIntegerField()
    rank_value = models.BigIntegerField()
    rank_volume = models.BigIntegerField()

class users(models.Model):
    id = models.AutoField(primary_key=True)
    username = models.TextField()
    password = models.TextField()
    crypto = models.TextField()
    line_id = models.TextField()
    is_login = models.BigIntegerField()
    status = models.BigIntegerField()
    noti_status = models.BigIntegerField()
    daily_status = models.BigIntegerField()
    date = models.DateTimeField()

class cryptocurrency_profile(models.Model):
    id = models.AutoField(primary_key=True)
    crypto = models.TextField()
    name = models.TextField()
    details = models.TextField()
    background_details = models.TextField()
    individuals = models.TextField()
    organizations = models.TextField()
    technology_details = models.TextField()
    initial_supply = models.TextField()
    allocated_to_investors_percentage = models.TextField()
    allocated_to_organization_or_founders_percentage = models.TextField()
    allocated_to_premined_rewards_or_airdrops_percentage = models.TextField()

class cryptocurrency_value_one(models.Model):
    id = models.AutoField(primary_key=True)
    crypto = models.TextField()
    price_usd = models.FloatField()
    volume = models.FloatField()
    price_usd_percentage = models.FloatField()
    volume_percentage = models.FloatField()
    date_time = models.DateTimeField()
    status = models.IntegerField()

class questions(models.Model):
    id = models.AutoField(primary_key=True)
    line_id = models.TextField()
    username = models.TextField()
    question = models.TextField()
    answer = models.TextField()
    date_time = models.TextField()
    status = models.IntegerField()

class trade(models.Model):
    id = models.AutoField(primary_key=True)
    trade_time = models.DateTimeField()
    trade_time_unix = models.TextField()
    symbol = models.TextField()
    side = models.TextField()
    price = models.FloatField()
    size = models.FloatField()
    
class foot_print(models.Model):
    id = models.AutoField(primary_key=True)
    line_id = models.TextField()
    command = models.TextField()
    date_time = models.DateTimeField()

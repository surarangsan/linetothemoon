from email import message
import json
import requests
import re
import pytz
from crypto.models import *
from asyncio.windows_events import NULL
from django.shortcuts import render
from django.http import HttpResponse , JsonResponse
from crypto.views.Helpers import *
from crypto.models import news
from datetime import datetime, timedelta
from crypto.views.Helpers import Helper

class Task():
    
    def NewsApi(request): # Get News From API news
        url = "https://crypto-pulse.p.rapidapi.com/news"

        headers = {
            "X-RapidAPI-Host": "crypto-pulse.p.rapidapi.com",
            "X-RapidAPI-Key": "67b5b3eb4emsh8760bb5bd2f8765p117a48jsnae3650c0ce39"
        }

        response = requests.request("GET", url, headers=headers)
        responseText = json.loads(response.text)

        for item in responseText:
            dateTime = datetime.now(pytz.timezone('Asia/Bangkok'))
            value = item['date'].split(" ")
            if(value[1] == 'minutes'):
                dateTime = dateTime  -  timedelta(minutes=int(value[0]))
            elif(value[1] == 'hours'):
                dateTime = dateTime  -  timedelta(hours=int(value[0]))
            elif(value[1] == 'days'):
                dateTime = dateTime  -  timedelta(days=int(value[0]))
            else:
                dateTime = dateTime  -  timedelta(seconds=int(value[0]))
            insert = news()
            insert.topic  = item['title']
            insert.detail = item['description']
            insert.date_time  = dateTime.strftime('%Y-%m-%d %H:%M:%S')
            insert.source = item['source']
            insert.link = item['link']
            insert.status = 0
            insert.save()
        return HttpResponse(status=200)

    def MarketCapFifteenApi(request): # Get API crypto value every 15 mins
        dataValueDictUpdate = dict()
        dataVolumeDictUpdate = dict()
        dataCryptocurrency = cryptocurrency.objects.all().values()
        for itemCrypto in dataCryptocurrency:
            checkCryptocurrencyValue = cryptocurrency_value_fifteen.objects.all().filter(crypto=itemCrypto['name_short']).values()
            if(len(checkCryptocurrencyValue) == 0):

                url = "https://data.messari.io/api/v1/assets/"+itemCrypto['name_short']+"/metrics"

                payload={}
                headers = {'x-messari-api-key': 'd7fbbd59-1872-4b6f-8cfb-4ec78861c272'}

                response = requests.request("GET", url, headers=headers, data=payload)
                responseText = json.loads(response.text)
                if(response.status_code == 200):
                    dataCryptocurrencyValue = cryptocurrency_value_fifteen()
                    dataCryptocurrencyValue.crypto = str(responseText['data']['symbol']).lower()
                    dataCryptocurrencyValue.price_usd = float(responseText['data']['market_data']['price_usd'])
                    dataCryptocurrencyValue.market_cap = float(responseText['data']['marketcap']['current_marketcap_usd'])
                    dataCryptocurrencyValue.volume = float(responseText['data']['market_data']['real_volume_last_24_hours'])
                    dataCryptocurrencyValue.date_time =  datetime.now(pytz.timezone('Asia/Bangkok')).strftime('%Y-%m-%d %H:%M:%S')
                    dataCryptocurrencyValue.price_usd_percentage = 0
                    dataCryptocurrencyValue.volume_percentage = 0
                    dataCryptocurrencyValue.status = 0
                    dataCryptocurrencyValue.save()

                    print("SUCCESS : "+str(itemCrypto['name_short']).lower())
                else :  
                    print("FAILED : "+str(itemCrypto['name_short']).lower())
            else:

                dataValue = cryptocurrency_value_fifteen.objects.all().filter(crypto=itemCrypto['name_short']).extra(order_by = ['-id'])[:1].values()
                
                url = "https://data.messari.io/api/v1/assets/"+itemCrypto['name_short']+"/metrics"

                payload={}
                headers = {'x-messari-api-key': 'd7fbbd59-1872-4b6f-8cfb-4ec78861c272'}

                response = requests.request("GET", url, headers=headers, data=payload)
                responseText = json.loads(response.text)
                if(response.status_code == 200):

                    dataCryptocurrencyValue = cryptocurrency_value_fifteen()
                    dataCryptocurrencyValue.crypto = str(responseText['data']['symbol']).lower()
                    dataCryptocurrencyValue.price_usd = float(responseText['data']['market_data']['price_usd'])
                    dataCryptocurrencyValue.market_cap = float(responseText['data']['marketcap']['current_marketcap_usd'])
                    dataCryptocurrencyValue.volume = float(responseText['data']['market_data']['real_volume_last_24_hours'])
                    dataCryptocurrencyValue.date_time =  datetime.now(pytz.timezone('Asia/Bangkok')).strftime('%Y-%m-%d %H:%M:%S')
                    dataCryptocurrencyValue.price_usd_percentage = float("{:.2f}".format(((float(responseText['data']['market_data']['price_usd'] - dataValue[0]['price_usd']))/dataValue[0]['price_usd']) *100))
                    dataCryptocurrencyValue.volume_percentage = float("{:.2f}".format(((float(responseText['data']['market_data']['real_volume_last_24_hours'] - dataValue[0]['volume']))/dataValue[0]['volume']) *100))
                    dataCryptocurrencyValue.status = 0
                    dataCryptocurrencyValue.save()

                    dataValueDictUpdate[str(responseText['data']['symbol']).lower()] = dataCryptocurrencyValue.price_usd_percentage
                    dataVolumeDictUpdate[str(responseText['data']['symbol']).lower()] = dataCryptocurrencyValue.volume_percentage

                    print("SUCCESS : "+str(itemCrypto['name_short']).lower())
                else :  
                    print("FAILED : "+str(itemCrypto['name_short']).lower())

            dataValueDictSort = dict(sorted(dataValueDictUpdate.items(), key=lambda item: item[1],reverse=True))
            countValue = 0
            for item in dataValueDictSort:
                countValue = countValue + 1

            dataVolumeDictSort = dict(sorted(dataVolumeDictUpdate.items(), key=lambda item: item[1],reverse=True))
            countValue = 0
            for item in dataVolumeDictSort:
                countValue = countValue + 1
                
            dataCryptocurrencyUpdate = cryptocurrency.objects.all().get(name_short=itemCrypto['name_short'])
            dataCryptocurrencyUpdate.rank_volume = countValue
            dataCryptocurrencyUpdate.rank_value = countValue
            dataCryptocurrencyUpdate.save()

        return HttpResponse(200)

    def MaketCapOneApi(request): # Get API crypto value every 1 mins
        dataValueDictUpdate = dict()
        dataVolumeDictUpdate = dict()
        dataCryptocurrency = cryptocurrency.objects.all().values()
        for item in dataCryptocurrency:
            checkCryptocurrencyValue = cryptocurrency_value_one.objects.all().filter(crypto=item['name_short']).values()
            if(len(checkCryptocurrencyValue) == 0):
                url = "https://data.messari.io/api/v1/assets/"+item['name_short']+"/metrics"

                payload={}
                headers = {'x-messari-api-key': 'd7fbbd59-1872-4b6f-8cfb-4ec78861c272'}

                response = requests.request("GET", url, headers=headers, data=payload)
                responseText = json.loads(response.text)
                if(response.status_code == 200):

                    dataCryptocurrencyValue = cryptocurrency_value_one()
                    dataCryptocurrencyValue.crypto = str(responseText['data']['symbol']).lower()
                    dataCryptocurrencyValue.price_usd = float(responseText['data']['market_data']['price_usd'])
                    dataCryptocurrencyValue.volume = float(responseText['data']['market_data']['real_volume_last_24_hours'])
                    dataCryptocurrencyValue.date_time =  datetime.now(pytz.timezone('Asia/Bangkok')).strftime('%Y-%m-%d %H:%M:%S')
                    dataCryptocurrencyValue.price_usd_percentage = 0
                    dataCryptocurrencyValue.volume_percentage = 0
                    dataCryptocurrencyValue.status = 0
                    dataCryptocurrencyValue.save()
                    print("SUCCESS : "+str(item['name_short']).lower())
                else :  
                    print("FAILED : "+str(item['name_short']).lower())
            else:
                dataValue = cryptocurrency_value_one.objects.all().filter(crypto=item['name_short']).extra(order_by = ['-id'])[:1].values()
                url = "https://data.messari.io/api/v1/assets/"+item['name_short']+"/metrics"

                payload={}
                headers = {'x-messari-api-key': 'd7fbbd59-1872-4b6f-8cfb-4ec78861c272'}

                response = requests.request("GET", url, headers=headers, data=payload)
                responseText = json.loads(response.text)
                if(response.status_code == 200):

                    dataCryptocurrencyValue = cryptocurrency_value_one()
                    dataCryptocurrencyValue.crypto = str(responseText['data']['symbol']).lower()
                    dataCryptocurrencyValue.price_usd = float(responseText['data']['market_data']['price_usd'])
                    dataCryptocurrencyValue.volume = float(responseText['data']['market_data']['real_volume_last_24_hours'])
                    dataCryptocurrencyValue.date_time =  datetime.now(pytz.timezone('Asia/Bangkok')).strftime('%Y-%m-%d %H:%M:%S')
                    dataCryptocurrencyValue.price_usd_percentage = float("{:.2f}".format(((float(responseText['data']['market_data']['price_usd'] - dataValue[0]['price_usd']))/dataValue[0]['price_usd']) *100))
                    dataCryptocurrencyValue.volume_percentage = float("{:.2f}".format(((float(responseText['data']['market_data']['real_volume_last_24_hours'] - dataValue[0]['volume']))/dataValue[0]['volume']) *100))
                    dataCryptocurrencyValue.status = 0
                    dataCryptocurrencyValue.save()

                    dataValueDictUpdate[str(responseText['data']['symbol']).lower()] = dataCryptocurrencyValue.price_usd_percentage
                    dataVolumeDictUpdate[str(responseText['data']['symbol']).lower()] = dataCryptocurrencyValue.volume_percentage

                    print("SUCCESS : "+str(item['name_short']).lower())
                else :  
                    print("FAILED : "+str(item['name_short']).lower())

            dataValueDictSort = dict(sorted(dataValueDictUpdate.items(), key=lambda item: item[1],reverse=True))
            countValue = 0
            for item in dataValueDictSort:
                countValue = countValue + 1

            dataVolumeDictSort = dict(sorted(dataVolumeDictUpdate.items(), key=lambda item: item[1],reverse=True))
            countValue = 0
            for item in dataVolumeDictSort:
                countValue = countValue + 1
      
            dataCryptocurrencyUpdate = cryptocurrency.objects.all().get(name_short=item)
            dataCryptocurrencyUpdate.rank_volume = countValue
            dataCryptocurrencyUpdate.rank_value = countValue
            dataCryptocurrencyUpdate.save()  

        return HttpResponse(200)

    def CryptoNotiPriceApi(request): # Create message before send to user
        message = ""
        dataUser = users.objects.all().filter(noti_status=1).values()
        for itemUser in dataUser :
            lineId = itemUser['line_id']
            cryptoArr = itemUser['crypto'].replace('(','').replace(')','')[:-1]
            cryptoArr = cryptoArr.split(',')
            message = ""
            for itemCrypto in cryptoArr:
                
                data = cryptocurrency_value_fifteen.objects.all().filter(crypto=itemCrypto).filter(id__gte=756866).order_by('-id').values()
                price = str(data[0]['price_usd'])
                message = message + itemCrypto.upper() +" : $" + Helper.FormatPrice(price) +"\n"
                messagePrediction = ""
                prediction = Helper.GetPriceToPredictFromDb(itemCrypto,4,"price_usd")
                start = data[0]['price_usd']
                dateTime = datetime.now()
                for item in prediction :
                    dateTime = dateTime + timedelta(minutes=15)
                    time = dateTime.strftime("%H:%M:%S")
                    if(start < item):
                        messagePrediction = messagePrediction +"⏫ T : "+ time + " P : $" + Helper.FormatPrice(item) + "\n"
                    else:
                        messagePrediction = messagePrediction +"⏬ T : "+ time + " P : $" + Helper.FormatPrice(item) +"\n"
                    start = item
                
                if(float(price) < float(prediction[len(prediction)-1])):
                    scoreCard = "🟢"
                elif(float(price) > float(prediction[len(prediction)-1])):
                    scoreCard = "🔴"
                else:
                    scoreCard = "🟡"
                messagePrediction = "Prediction : "+scoreCard+" \n" + messagePrediction
                message = message + messagePrediction + "\n"
                lastUpdate = data[0]['date_time'].strftime('%d-%m-%Y %H:%M:%S')

            message = "Last updated : \n" + lastUpdate +"\n"+ message
            Helper.SendLineNoti(lineId, message)

        return HttpResponse(200)

    def CryptoNotiPriceDailyApi(request): # Create Summary every day
        dataCryptocurrency = cryptocurrency.objects.all().values()

        for item in dataCryptocurrency:
        
            url = "https://data.messari.io/api/v1/assets/"+item['name_short']+"/metrics"

            payload={}
            headers = {'x-messari-api-key': 'd7fbbd59-1872-4b6f-8cfb-4ec78861c272'}
            
            response = requests.request("GET", url, headers=headers, data=payload)
            responseText = json.loads(response.text)

            open = responseText['data']['market_data']['ohlcv_last_24_hour']['open']
            latestData = responseText['data']['market_data']['price_usd']
            growRate = ((latestData - open)/open) * 100

            dataCryptocurrencyValueDaily = cryptocurrency_value_daily()
            dataCryptocurrencyValueDaily.crypto = str(item['name_short']).lower()
            dataCryptocurrencyValueDaily.grow_rate = growRate
            dataCryptocurrencyValueDaily.close = latestData
            dataCryptocurrencyValueDaily.open = open
            dataCryptocurrencyValueDaily.date_time = datetime.now(pytz.timezone('Asia/Bangkok')).strftime('%Y-%m-%d %H:%M:%S')
            dataCryptocurrencyValueDaily.status = 0
            dataCryptocurrencyValueDaily.save()
                
            dataCryptocurrencyUpdate = cryptocurrency_value_one.objects.filter(crypto=str(item['name_short']).lower())
            dataCryptocurrencyUpdate.update(status=1)
        
        return HttpResponse(200)

    def CryptoRankApi(request): # Create Message Summary crypto
        data = cryptocurrency_value_daily.objects.all().extra(order_by = ['-id'])[:10].values()
        idCryptoArray = []
        for itemVolume in  data :
            idCryptoArray.append(itemVolume['id'])
        dataVolume = cryptocurrency_value_daily.objects.all().filter(id__in=idCryptoArray).extra(order_by = ['-grow_rate'])[:5].values()
        count = 0
        message = "5 อันดับอัตราการเติบโตสูงสุดของเหรียญ"
        # dateTime = datetime.now(pytz.timezone('Asia/Bangkok')).strftime('%d-%m-%Y')
        message 
        for itemVolume in dataVolume :
            count = count + 1
            message = message +"\n"+ str(count) + ". " + str(itemVolume['crypto']).upper()
            message = message +" \n ราคา : $" + Helper.FormatPrice(itemVolume['close'])
            message = message +" \n อัตราการเติบโต : " + str("%.2f" % itemVolume['grow_rate']) +"%"
            message = message +" \n เวลา : " + itemVolume['date_time'].strftime('%d-%m-%Y %H:%M:%S')
    
        dataUser = users.objects.all().filter(daily_status=1).values()
        dataCryptocurrency = cryptocurrency.objects.all().values()
        dataCryptocurrencyArr = {}
        for itemValue in dataCryptocurrency :
            dataCryptocurrencyArr[itemValue['name_short']] = 0
        for itemValue in dataUser :
            cryptoArr = itemValue['crypto'].replace('(','').replace(')','')[:-1]
            cryptoArr = cryptoArr.split(',')
            for itemCrypto in cryptoArr :
                if(len(itemCrypto) != 0):
                    dataCryptocurrencyArr[str(itemCrypto)] = dataCryptocurrencyArr[str(itemCrypto)] + 1
                        
        dataCryptocurrencyArrSort = sorted(dataCryptocurrencyArr.items(), key=lambda x: x[1], reverse=True)
        message = message + "\nเหรียญที่มีผู้ใช้งานติดตามมากที่สุด : "
        count = 0
        del dataCryptocurrencyArrSort[5:]
        for itemCryptocurrencyArrSort in dataCryptocurrencyArrSort :
            count = count + 1
            message = message + "\n" + str(count) + ". " + itemCryptocurrencyArrSort[0] + " จำนวน : " + str(itemCryptocurrencyArrSort[1])

        for itemUser in dataUser :
            Helper.SendLineNoti(itemUser['line_id'], message)

        return HttpResponse(200)

    def CryptoProfileApi(request): # Get API crypto
        dataCryptocurrency = cryptocurrency.objects.all().values()
        for item in dataCryptocurrency:
            checkCryptocurrencyProfile = cryptocurrency_profile.objects.all().filter(crypto=item['name_short']).values()
            if(len(checkCryptocurrencyProfile) == 0):
                url = "https://data.messari.io/api/v2/assets/"+item['name_short']+"/profile"

                payload={}
                headers = {'x-messari-api-key': 'd7fbbd59-1872-4b6f-8cfb-4ec78861c272'}

                response = requests.request("GET", url, headers=headers, data=payload)
                responseText = json.loads(response.text)
                if(response.status_code == 200):
                    cryptocurreccyprofile = cryptocurrency_profile()
                    cryptocurreccyprofile.crypto = responseText['data']['symbol'].lower()
                    cryptocurreccyprofile.name = responseText['data']['name']
                    if(responseText['data']['profile']['general']['overview']['project_details']):
                        cryptocurreccyprofile.details = Helper.RemoveHtml(responseText['data']['profile']['general']['overview']['project_details'])
                    if(responseText['data']['profile']['general']['background']['background_details']):
                        cryptocurreccyprofile.background_details = Helper.RemoveHtml(responseText['data']['profile']['general']['background']['background_details'])
                    individuals = responseText['data']['profile']['contributors']['individuals']
                    value = ""
                    if len(individuals):
                        for item in individuals :
                            value = value +"("+ item['slug'].replace("-"," ") +"),"
                        cryptocurreccyprofile.individuals = value
                    else:
                        cryptocurreccyprofile.individuals = ""

                    value = ""
                    organizations = responseText['data']['profile']['contributors']['organizations']
                    if len(organizations):
                        for item in organizations :
                            value = value +"("+ item['slug'].replace("-"," ") +"),"
                        cryptocurreccyprofile.organizations = value
                    else:
                        cryptocurreccyprofile.organizations = ""
                    if(responseText['data']['profile']['technology']['overview']['technology_details']):
                        cryptocurreccyprofile.technology_details = Helper.RemoveHtml(responseText['data']['profile']['technology']['overview']['technology_details'])
                    if (responseText['data']['profile']['economics']["launch"]['initial_distribution']['initial_supply']):                
                        cryptocurreccyprofile.initial_supply = responseText['data']['profile']['economics']["launch"]['initial_distribution']['initial_supply']
                    else :
                        cryptocurreccyprofile.initial_supply = 0
                    if(responseText['data']['profile']['economics']["launch"]['initial_distribution']['initial_supply_repartition']['allocated_to_investors_percentage']):
                        cryptocurreccyprofile.allocated_to_investors_percentage = responseText['data']['profile']['economics']["launch"]['initial_distribution']['initial_supply_repartition']['allocated_to_investors_percentage']
                    else :
                        cryptocurreccyprofile.allocated_to_investors_percentage = 0
                    if(responseText['data']['profile']['economics']["launch"]['initial_distribution']['initial_supply_repartition']['allocated_to_organization_or_founders_percentage']):
                        cryptocurreccyprofile.allocated_to_organization_or_founders_percentage = responseText['data']['profile']['economics']["launch"]['initial_distribution']['initial_supply_repartition']['allocated_to_organization_or_founders_percentage']
                    else:
                        cryptocurreccyprofile.allocated_to_organization_or_founders_percentage = 0
                    if(responseText['data']['profile']['economics']["launch"]['initial_distribution']['initial_supply_repartition']['allocated_to_premined_rewards_or_airdrops_percentage']):
                        cryptocurreccyprofile.allocated_to_premined_rewards_or_airdrops_percentage = responseText['data']['profile']['economics']["launch"]['initial_distribution']['initial_supply_repartition']['allocated_to_premined_rewards_or_airdrops_percentage']
                    else:
                        cryptocurreccyprofile.allocated_to_premined_rewards_or_airdrops_percentage = 0
                    
                    cryptocurreccyprofile.save()

        return HttpResponse(200)

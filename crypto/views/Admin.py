from django.shortcuts import render
from crypto.models import *
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.db.models import Count
from django.core.paginator import Paginator
from crypto.views.Helpers import Helper

class Admin():

    def Login(request):
        return render(request, 'admin/login.html')
    
    def News(request):
        data = news.objects.all().order_by('-date_time').values()

        dataPaginator = Paginator(data, 10)

        page_num = request.GET.get('page')

        page = dataPaginator.get_page(page_num)

        return render(request, 'admin/news.html',{'page':page})

    def NewsDetail(request):
        id = request.GET.get('id')
        data = news.objects.all().filter(id=id).values()
        return render(request, 'admin/newsDetail.html',{'data':data[0]})

    def Home(request):
        footPrint = foot_print.objects.values('command').annotate(count=Count('id'))
        footPrintCommand = []
        footPrintCommandCount = []
        for item in footPrint:
            footPrintCommand.append(item['command'])
            footPrintCommandCount.append(int(item['count']))

        dataCrypto = cryptocurrency.objects.all().values()
        dataValue = cryptocurrency_value_fifteen.objects.all().values()
        dataValueDaily = cryptocurrency_value_daily.objects.all().values()
        dataCryptoProfile = cryptocurrency_profile.objects.all().values()
        dataNews = news.objects.all().values()
        dataUsers = users.objects.all().values()
        dataUsersActive = users.objects.all().filter(status=1).values()
        dataUsersDisActive = users.objects.all().filter(status=0).values()
        dataFootPrint = foot_print.objects.all().values()
        cryptoDataRow = []
        numberDataRow = []
        popularData = []
        popular = {}
        cryptoDataNumber = len(dataCrypto)
        dataValueNumber = len(dataValue)
        dataValueDailyNumber = len(dataValueDaily)
        dataCryptoProfileNumber = len(dataCryptoProfile)
        dataUsersNumber = len(dataUsers)
        dataNews = len(dataNews)
        dataUsersActiveNumber = len(dataUsersActive)
        dataUsersDisActiveNumber = len(dataUsersDisActive)
        dataFootPrintNumber = len(dataFootPrint)
        for item in dataCrypto:
            dataValue = cryptocurrency_value_fifteen.objects.all().filter(crypto=item['name_short']).values()
            cryptoDataRow.append(item['name_short'])
            numberDataRow.append(len(dataValue))
            popular[item['name_short']] = 0

        for itemValue in dataUsers :
            cryptoArr = itemValue['crypto'].replace('(','').replace(')','')[:-1]
            cryptoArr = cryptoArr.split(',')
            for itemCrypto in cryptoArr :
                if(len(itemCrypto) != 0):
                    popular[str(itemCrypto)] = popular[str(itemCrypto)] + 1

        for item in popular:
            popularData.append(popular[str(item)])

        
        return render(request, 'admin/home.html',{'cryptoDataRow': cryptoDataRow,'numberDataRow' : numberDataRow, 'popularData' : popularData, 'cryptoDataNumber':cryptoDataNumber
,'dataValueNumber' : dataValueNumber, 'dataValueDailyNumber':dataValueDailyNumber, 'dataCryptoProfileNumber':dataCryptoProfileNumber
,'dataNews':dataNews, 'dataUsersNumber':dataUsersNumber,'dataUsersActiveNumber':dataUsersActiveNumber,'dataUsersDisActiveNumber':dataUsersDisActiveNumber
,'dataFootPrintNumber':dataFootPrintNumber,'footPrintCommand':footPrintCommand,'footPrintCommandCount':footPrintCommandCount})
    
    @csrf_exempt
    def CheckLogin(request):
        usernameLogin = request.POST.get('username')
        passwordLogin = request.POST.get('password')
        # password = 'Ufe56503cd2d0b7b0cb0fe4a68a8a11ea'
        password = '123456789'

        user = users.objects.all().filter(username=usernameLogin).values()
        if(len(user) != 0):
            if(user[0]['username'] == usernameLogin and password == passwordLogin):
                return HttpResponse(status=200)
            else:
                return HttpResponse(status=403)
        else:
            return HttpResponse(status=403)

    def Crypto(request):
        data = cryptocurrency.objects.all().values()
        return render(request, 'admin/crypto.html',{'data':data})

    @csrf_exempt
    def DaleteCrypto(request):
        id = request.POST.get('id', '')
        data = cryptocurrency.objects.all().filter(id=id).values()
        if(len(data) != 0):
            Helper.DeleteImage(data[0]['image'])
            cryptocurrency.objects.filter(id=id).delete()
            return HttpResponse(status=200)
        else:
            return HttpResponse(status=500)

    @csrf_exempt
    def EditCrypto(request):
        id = request.POST.get('id', '')
        name = request.POST.get('name', '')
        name_short = request.POST.get('name_short', '')
        if 'image' in request.FILES:
            path = Helper.DownloadImage(request.FILES['image'],name_short)
        data = cryptocurrency.objects.all().filter(id=id).values()
        if(len(data) != 0):
            update = cryptocurrency.objects.get(id=id)
            update.name = name
            update.name_short = name_short
            if 'image' in request.FILES:
                update.image = path
            update.save()
            return HttpResponse(status=200)
        else:
            return HttpResponse(status=500)

    @csrf_exempt
    def CreateCrypto(request):
        crypto = request.POST.get('crypto', '')
        name = request.POST.get('name', '')
        name_short = request.POST.get('name_short', '')
        if 'image' in request.FILES:
            path = Helper.DownloadImage(request.FILES['image'],name_short)
        data = cryptocurrency.objects.all().filter(name_short=crypto).values()
        if(len(data) == 0):
            insert = cryptocurrency()
            insert.name = name
            insert.name_short = name_short
            if 'image' in request.FILES:
                insert.image = path
            insert.save()
            return HttpResponse(status=200)
        else:
            return HttpResponse(status=500)

    @csrf_exempt
    def DaleteNews(request):
        id = request.POST.get('id', '')
        data = news.objects.all().filter(id=id).values()
        if(len(data) != 0):
            news.objects.filter(id=id).delete()
            return HttpResponse(status=200)
        else:
            return HttpResponse(status=500)

    def Users(request):
        data = users.objects.all().values()
        return render(request, 'admin/user.html', {'data' : data})

    @csrf_exempt
    def DaleteUsers(request):
        id = request.POST.get('id', '')
        data = users.objects.all().filter(id=id).values()
        if(len(data) != 0):
            users.objects.filter(id=id).delete()
            return HttpResponse(status=200)
        else:
            return HttpResponse(status=500)

    @csrf_exempt
    def EditUsers(request):
        id = request.POST.get('id', '')
        username = request.POST.get('username', '')
        line_id = request.POST.get('line_id', '')
        crypto = request.POST.get('crypto', '')
        status = request.POST.get('status', '')
        data = users.objects.all().filter(id=id).values()
        if(len(data) != 0):
            update = users.objects.get(id=id)
            update.username = username
            update.line_id = line_id
            update.crypto = crypto
            update.status = status
            update.save()

            return HttpResponse(status=200)
        else:
            return HttpResponse(status=500)

    @csrf_exempt
    def CreateUsers(request):
        username = request.POST.get('username', '')
        line_id = request.POST.get('line_id', '')
        crypto = request.POST.get('crypto', '')
        status = request.POST.get('status', '')
        data = users.objects.all().filter(line_id=line_id).values()
        if(len(data) == 0):
            insert = users()
            insert.username = username
            insert.line_id = line_id
            insert.crypto = crypto
            insert.status = status
            insert.save()

            return HttpResponse(status=200)
        else:
            return HttpResponse(status=500)

    def Questions(request):
        question = questions.objects.all().order_by("status").values()
        return render(request, 'admin/questions.html',{'question':question})

    @csrf_exempt
    def DeleteQuestions(request):
        id = request.POST.get('id', '')
        data = questions.objects.all().filter(id=id).values()
        if(len(data) != 0):
            questions.objects.filter(id=id).delete()
            return HttpResponse(status=200)
        else:
            return HttpResponse(status=500)

    @csrf_exempt
    def SendQuestions(request):
        id = request.POST.get('id', '')
        question = request.POST.get('question', '')
        data = questions.objects.all().filter(id=id).values()
        if(len(data) != 0):
            update = questions.objects.get(id=id)
            update.answer = question
            update.status = 1
            update.save()

            Helper.SendLineNoti(data[0]['line_id'],"ผู้ดูแลระบบ : \n  "+question)
            return HttpResponse(status=200)
        else:
            return HttpResponse(status=500)


    
    




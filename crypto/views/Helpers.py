
from cgi import print_exception
import re
import json
import pytz
import requests
import matplotlib.pyplot as plt
from scipy import stats
from crypto.models import *
import logging
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn import metrics
import numpy as np
import pandas as pd
from sklearn import metrics
from sklearn.utils import shuffle
from django.core.files.storage import FileSystemStorage
import os
from pathlib import Path

BASE_DIR = Path(__file__).resolve().parent.parent


# -----------------------------------------------------------------------------------------------
# {assetKey} => ID (unique), slug (unique), or symbol (non-unique)
# https://data.messari.io/api/v1/assets ข้อมูลเบื้องต้นของทุกเหรียญ GET
# https://data.messari.io/api/v2/assets ข้อมูลเบื้องต้นของทุกเหรียญและราคา GET
# https://data.messari.io/api/v2/assets/{assetKey}/profile ข้อมูลเบื้องต้นของเหรียญ GET
# https://data.messari.io/api/v1/assets/{assetKey}/metrics การเงินของเหรียญ GET
# https://data.messari.io/api/v1/assets/{assetKey}/metrics/market-data การเงินของเหรียญในตลาด GET
# https://data.messari.io/api/v1/markets ราคาเหรียญ GET
# https://data.messari.io/api/v1/news ข่าวสารทั้งหมด GET
# https://data.messari.io/api/v1/news/{assetKey} ข่าวสารของเหรียญ GET
# https://www.youtube.com/watch?v=dONYOtb2ySI&list=PL6gx4Cwl9DGBlmzzFcLgDhKTTfNLfX1IK&index=8
# https://www.w3schools.com/python/python_ml_getting_started.asp
# https://towardsdatascience.com/step-by-step-guide-building-a-prediction-model-in-python-ac441e8b9e8b
# https://www.youtube.com/watch?v=jblzDafQG_8

# -----------------------------------------------------------------------------------------------
BearToken = "GVXedBd9XNu4gHYZtzAEZeSr4mExkYLTiFzw6dspmERQhPEKJH7dlAnJiqt1QJnKgdarufeadtgk0bR7fSSGnRu2apietRpX/K3nx4SKofGZKOCYSjrOEjsEw810AImFoFAsit/77Ig5/oK2m4SCmAdB04t89/1O/w1cDnyilFU="
logger = logging.getLogger('django')
class Helper():

    def Percentage(length, percentage):
        percentage = length * (percentage/100)
        return percentage

    def FormatPrice(price):
        price = float(price)
        checkDigi = str(price).split('.')
        if(len(checkDigi[0]) > 3):
            priceStr = str("%.2f" % price) 
            priceArr = str(priceStr).split('.')
            numbers = "{:,}".format(int(priceArr[0]))
            decimal = priceArr[1]
            price = numbers +'.'+decimal
        else:
            if(checkDigi[0] == 0):
                zero = len(re.search('\d+\.(0*)', str(price)).group(1))
            else:
                zero = len(re.search('\d+\.(0*)', str(price+1)).group(1))
            priceArr = str(price).split('.')
            if(zero == 0):
                priceDecimal = str("%.5f" % price)
            elif(zero <= 2):
                priceDecimal = str("%.5f" % price)
            elif(zero <= 4):
                priceDecimal = str("%.7f" % price)
            else:
                priceDecimal = str("%.10f" % price)
            
            price = priceDecimal

        return price

    def FormatNumber(number):
        number = int(number)
        number = "{:,}".format(number)
        return number

    def RemoveHtml(text):
        clean = re.compile('<.*?>')
        return re.sub(clean, '', text)

    def SendLineNoti(lineId, message):
        payload = json.dumps({
            "to": lineId,
            "messages": [
                {
                "type": "text",
                "text": message,
                }
            ]
        })

        url = "https://api.line.me/v2/bot/message/push"
        
        headers = {
            'Authorization': 'Bearer '+BearToken,
            'Content-Type': 'application/json'
        }

        response = requests.request("POST", url, headers=headers, data=payload)

        return response.text

    def ReplyLineNoti(ReplyId, message):
        payload = json.dumps({
            "replyToken": ReplyId,
            "messages":[
                {
                    "type":"text",
                    "text":message
                }
            ]
        })

        url = "https://api.line.me/v2/bot/message/reply"
        
        headers = {
            'Authorization': 'Bearer '+BearToken,
            'Content-Type': 'application/json'
        }

        response = requests.request("POST", url, headers=headers, data=payload)
        return response.text

    def SendLineLinkCryptoNoti(lineId, linkArr, buttonArr, title):
        url = "https://api.line.me/v2/bot/message/push"

        payload = json.dumps({
        "to": lineId,
        "messages": [
            {
            "type": "template",
            "altText": title,
            "template": {
                "type": "buttons",
                "text": title,
                "actions": [
                {
                    "type": "uri",
                    "label": buttonArr[0],
                    "uri": linkArr[0]
                },
                {
                    "type": "uri",
                    "label": buttonArr[1],
                    "uri": linkArr[1]
                }
                ]
            }
            }
        ]
        })
        headers = {
            'Authorization': 'Bearer '+BearToken,
            'Content-Type': 'application/json'
        }

        response = requests.request("POST", url, headers=headers, data=payload)

        return response.text

    def SendLineLinkNoti(lineId, link, button, title):
        url = "https://api.line.me/v2/bot/message/push"

        payload = json.dumps({
        "to": lineId,
        "messages": [
            {
            "type": "template",
            "altText": title,
            "template": {
                "type": "buttons",
                "text": title,
                "actions": [
                {
                    "type": "uri",
                    "label": button,
                    "uri": link
                }
                ]
            }
            }
        ]
        })
        headers = {
            'Authorization': 'Bearer '+BearToken,
            'Content-Type': 'application/json'
        }

        response = requests.request("POST", url, headers=headers, data=payload)
        return response

    def  GetPriceToPredictFromDb(crypto, timeFrame, type):
        x = []
        y = []
        data = cryptocurrency_value_fifteen.objects.all().filter(crypto=crypto).order_by('-id').values()
        for item in data :
            y.append(float(item[type]))
            x.append(int(item['date_time'].timestamp()))

        prediction = Helper.Prediction(x, y, timeFrame)

        return prediction

    def  GetPriceDailyToPredictFromDb(crypto, timeFrame, type):
        y = []
        x = []
        data = cryptocurrency_value_daily.objects.all().filter(crypto=crypto).order_by('-id').values()
        for item in data :
            y.append(float(item[type]))
            x.append(int(item['date_time'].timestamp()))

        PredictionDaily = Helper.PredictionDaily(x, y, timeFrame)

        return PredictionDaily

    def Prediction(x, y, timeTrame):
        predictionTime = 15
        secound = 60
        prediction = []
        unixTime = x[0]

        for index in range(timeTrame):
            unixTime = unixTime + (predictionTime * secound)
            slope, intercept, relation, p, standard_error = stats.linregress(x, y)
            predictionValue = slope * unixTime + intercept
            x.insert(0, unixTime)
            y.insert(0, predictionValue)
            prediction.append(predictionValue)

        return prediction
    
    def PredictionDaily(x, y, timeTrame):
        predictionTime = 1440
        secound = 60
        prediction = []
        unixTime = x[0]

        for index in range(timeTrame):
            unixTime = unixTime + (predictionTime * secound)
            slope, intercept, relation, p, standard_error = stats.linregress(x, y)
            predictionValue = slope * unixTime + intercept
            x.insert(0, unixTime)
            y.insert(0, predictionValue)
            prediction.append(predictionValue)

        return prediction
    
    def FootPrint(line_id, command):
        footPrint = foot_print()
        footPrint.line_id = line_id
        footPrint.command = command
        footPrint.date_time = datetime.now(pytz.timezone('Asia/Bangkok')).strftime('%Y-%m-%d %H:%M:%S')
        footPrint.save()

    def CheckModel(request):
        crypto = cryptocurrency.objects.all().values()
        for itemCrypto in crypto:
            x = []
            y = []
            cryptoPrice = cryptocurrency_value_fifteen.objects.all().filter(crypto=itemCrypto['name_short']).values()
            for item in cryptoPrice:
                y.append(item['price_usd'])
                x.append(int(item['date_time'].timestamp()))

            df = pd.DataFrame({'x':x, 'y':y})

            x = df[['x']]
            y = df[['y']]

            # 80% - 20%
            xTrain,xTest,yTrain,yTest =train_test_split(x,y,test_size=0.2,random_state=0)

            #training
            model=LinearRegression()
            model.fit(xTrain,yTrain)

            #test
            y_pred=model.predict(xTest)

            # Mean Absolute Error
            mae = metrics.mean_absolute_error(yTest,y_pred)

            # Mean Square Error
            mse = metrics.mean_squared_error(yTest,y_pred)

            # Root Mean Square Error
            rmse = np.sqrt(metrics.mean_squared_error(yTest,y_pred))

            score = metrics.r2_score(yTest,y_pred)

            print("----------------------")
            print(itemCrypto['name_short'])
            print("MAE = " + str(mae))
            print("MSE = " + str(mse))
            print("RMSE = " + str(rmse))
            print("Score = " + str(score))

    def  GetPriceToPredictFromDbGraph(coin, predict, limit):
        x = []
        y = []
        z = []
        data = cryptocurrency_value_fifteen.objects.all().filter(crypto=coin).filter(id__gte=756866).order_by('-id').values()
        dataCoin = cryptocurrency.objects.all().filter(name_short=coin).values()
        for item in reversed(data) :
            y.append(float(item['price_usd']))
            x.append(int(item['date_time'].timestamp()))
            z.append(float(item['volume']))
        i = dataCoin[0]['image']
        m = data[0]['market_cap']
        unixTime = x[len(x)-(int(limit)-1)]
        prediction = Helper.PredictionGraph(m,x, y, z, i, unixTime, dataCoin[0]['name'],dataCoin[0]['name_short'], limit, predict)


        return prediction
    
    def PredictionGraph(m, x, y, z, i, unixTime, name, shortName, limit, predict):
        predictionTime = 15
        secound = 60
        result = {}

        df = pd.DataFrame({'x':x, 'y':y})

        dataX = df[['x']].values.reshape(-1,1)
        xTrain = df[['x']][int(Helper.Percentage(len(x),20)):].values.reshape(-1,1)
        xTest = df[['x']][int(Helper.Percentage(len(x),20)):].values.reshape(-1,1)

        dataY = df['y'].values.reshape(-1,1)
        yTrain = df[['y']][int(Helper.Percentage(len(y),20)):].values.reshape(-1,1)
        yTest = df[['y']][int(Helper.Percentage(len(y),20)):].values.reshape(-1,1)
        
        xTestShuffle, yTestShuffle = shuffle(xTest, yTest , random_state=1)

        #training
        model=LinearRegression()
        model.fit(xTrain,yTrain)

        #test
        yPred=model.predict(xTestShuffle)

        mse = np.sqrt(metrics.mean_squared_error(yTest,yPred))

        rSquare = model.score(dataX,dataY) 

        actually = np.array(dataY.flatten())[-limit:]
        predicated = np.array(yPred.flatten())[-limit:]
        
        value = []
        lastest = unixTime
        date_time = []
        for item in range(limit):
            date_time.append(datetime.fromtimestamp(lastest, pytz.timezone("Asia/Bangkok")).strftime('%d/%m/%Y %H:%M:%S'))
            lastest = lastest + (secound * predictionTime)
            if(predicated[item] > actually[item]):
                value.append(predicated[item] - mse)
            else:
                value.append(predicated[item] + mse)


        for item in range(predict):
            lastest = lastest + (secound * predictionTime)
            date_time.append(datetime.fromtimestamp(lastest, pytz.timezone("Asia/Bangkok")).strftime('%d/%m/%Y %H:%M:%S'))
            yPred = model.predict([[lastest]])
            lastActualValue = [[actually[len(actually)-1]]]
            rmse = metrics.mean_squared_error(lastActualValue,yPred)
            if(yPred > predicated[len(predicated)-1]):
                value.append(yPred[0][0] - rmse)
            else:
                value.append(yPred[0][0] + rmse)
           
        result['coin']  = name
        result['short_name']  = shortName
        result['r_square'] = int(rSquare * 100)
        result['predicted'] = value[-limit-predict:]
        result['actually'] = y[-limit:]
        result['volume'] = z[-limit:]
        result['date_time']  = date_time
        result['market_cap']  = m
        result['image']  = i
        return result
       
    def DownloadImage(file,fileName):
        path = 'uploads/'
        fileMimeType = (file.content_type).split("/")[1]
        check = os.path.exists(path+fileName+"."+fileMimeType)
        if(check):
            os.remove("C:\inetpub\wwwroot\linetothemoon\media\\"+path+fileName+"."+fileMimeType)
        fs = FileSystemStorage()
        file = fs.save(path+fileName+"."+fileMimeType, file)
        fileurl = fs.url(file)
        return fileurl

    def DeleteImage(file):
       os.remove("C:\inetpub\wwwroot\linetothemoon\\"+file)
       
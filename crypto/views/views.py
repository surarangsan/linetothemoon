
import json
import warnings
import pytz
from crypto.models import *
import pandas as pd
from asyncio.windows_events import NULL
from django.shortcuts import render
from django.http import HttpResponse , JsonResponse
from crypto.views.Helpers import *
from django.views.decorators.csrf import csrf_exempt
from datetime import datetime, timedelta
import logging
from crypto.views.Helpers import Helper
from django.core.paginator import Paginator

logger = logging.getLogger('django')
warnings.filterwarnings('ignore')
pd.options.display.float_format = '${:,.2f}'.format

#---------------------------------- Global Parameter ----------------------------------#
responseHttp = HttpResponse()

command = [
            "profile : แสดงข้อมูลส่วนบุคคล",
            "add {crypto short name} : เพิ่มเหรียญที่ต้องการติดตาม",
            "remove {crypto short name} : ลบเหรียญที่ติดตาม",
            "rank : แสดง 5 การเติบโตสูงสุดใน 15 นาที",
            "popular : แสดงเหรียญที่มีผู้ใช้งานติดตามมากที่สุด 5 อันดับ",
            "message {message} : ส่งข้อความไปหาผู้ดูแลระบบ",
            "dashboard : แสดงรายการภาพรวมของเหรียญในระบบ",
            "website : แสดงเหรียญและข้อมูลต่างๆพร้อมจัดการแก้แจ้งเตือน",
            "notify daily {on or off} : ตั้งค่าการแจ้งเตือนสรุปรายวัน",
            "notify {on or off} : ตั้งค่าการแจ้งเตือนราค่เหรียญทุก 15 นาที"
            ]
#---------------------------------- Global Parameter ----------------------------------#

#---------------------------------- Webhook ----------------------------------#
@csrf_exempt
def WebHook(request): # Get Message of user and reply (line bot)
    body = request.body.decode('utf-8')
    jsonBody = json.loads(body)
    message = ""
    lineId = jsonBody['events'][0]['source']['userId']
    dataUser = users.objects.all().filter(line_id=lineId).values()
    word = str(jsonBody['events'][0]['message']['text']).lower()
    checkLogin = word.split(" ")
    if (word.split(" ")[0] == 'admin'):     
        if(lineId == 'Ufe56503cd2d0b7b0cb0fe4a68a8a11ea'):
            Helper.SendLineLinkNoti(lineId,"https://liff.line.me/1656966432-g8MGQWyO","login","For Administation")
            return HttpResponse(200)
    if(checkLogin[0] == 'register'):
        Helper.FootPrint(lineId, "register")
        if (len(dataUser) == 0):
            Helper.FootPrint(lineId, "not register")
            message = ""
            message = message + "ไม่พบข้อมูลผู้ใช้งาน กรุณาลงทะเบียนที่ website"
            Helper.ReplyLineNoti(jsonBody['events'][0]['replyToken'], message)
            title =  '                  Website \n'
            Helper.SendLineLinkNoti(lineId, "https://liff.line.me/1656966432-4wrBkl0n", "see", title)
            return HttpResponse(200)
    if(len(dataUser) != 0):
        if  word == "profile":
            Helper.FootPrint(lineId, "profile")
            message = message + "Profile : \n" + dataUser[0]['username'] +"\n"
            message = message + "เหรียญที่ติดตาม : \n" + str(dataUser[0]['crypto']).replace(",", " ") +"\n"
            message = message + "วันที่เข้าเข้าใช้งาน : \n" + str(dataUser[0]['date'].strftime('%d-%m-%Y '))+"\n"
            if(dataUser[0]['noti_status'] == 1):
                status = "รับการแจ้งเตือน"
            else :
                status = "ไม่รับการแจ้งเตือน"
            message = message + "แจ้งเตือนราคาเหรียญทุก 15 นาที : \n" + status
            if(dataUser[0]['daily_status'] == 1):
                status = "รับการแจ้งเตือน"
            else :
                status = "ไม่รับการแจ้งเตือน"
            message = message + "\nแจ้งเตือนราคาสรุปยอดรายวัน : \n" + status
        elif word.split(" ")[0] == "notify" and word.split(" ")[1] == "daily":
            Helper.FootPrint(lineId, "notify")
            condition = word.split(" ")[2].lower()
            
            if condition == 'on':
                users.objects.all().filter(line_id=lineId).values().update(daily_status=1)
                message = "แจ้งเตือนราคาสรุปยอดรายวัน :\n"
                message = message + "รับการแจ้งเตือน"
            else:
                users.objects.all().filter(line_id=lineId).values().update(daily_status=0)
                message = ""
                message = message + "ไม่รับการแจ้งเตือน"     
        elif word.split(" ")[0] == "notify":
            Helper.FootPrint(lineId, "notify")
            condition = word.split(" ")[1].lower()
            if condition == 'on':
                users.objects.all().filter(line_id=lineId).values().update(noti_status=1)
                message = "แจ้งเตือนราคาเหรียญทุก 15 นาที :\n"
                message = message + "รับการแจ้งเตือน"
            else:
                users.objects.all().filter(line_id=lineId).values().update(noti_status=0)
                message = "แจ้งเตือนราคาเหรียญทุก 15 นาที :\n"
                message = message + "ไม่รับการแจ้งเตือน"
        elif word.split(" ")[0] == "add":
            Helper.FootPrint(lineId, "add")
            cryptoCurrent = dataUser[0]['crypto']
            crypto = word.split(" ")[1].replace(" ","")
            cryptoArr = cryptoCurrent.replace('(','').replace(')','')[:-1].split(',')
            if(len(cryptoArr) < 5):
                if "("+crypto+")," in dataUser[0]['crypto']:
                    message = "มีเหรียญนี้ '"+ crypto +"' ในบัญชีของคุณแล้ว \n"
                    message = message + "เหรียญที่ติดตาม : \n"
                    message = message + " " + dataUser[0]['crypto'].replace(",", " ")
                else:
                    checkCrypto = cryptocurrency.objects.all().filter(name_short=str(crypto).lower())
                    if(len(checkCrypto) != 0) :
                        cryptoCurrent = cryptoCurrent + "("+crypto+"),"
                        users.objects.all().filter(line_id=lineId).values().update(crypto=cryptoCurrent)
                        message = "ได้เพิ่มเหรียญ '"+ crypto +"' ในบัญชีของคุณแล้ว \n"
                        message = message + "เหรียญที่ติดตาม : \n"
                        message = message + " " + dataUser[0]['crypto'].replace(",", " ") + "("+crypto+")"
                    else:
                        message = "ไม่พบเหรียญ '"+ crypto +"' ในระบบ"
                        message = message + "เหรียญที่ติดตาม : \n"
                        message = message + " " + dataUser[0]['crypto'].replace(",", " ")
            else :
                    message = "เหรียญที่ติดตามเกินกำหนด (5 เหรียญต่อบัญชี) \n"
                    message = message + "เหรียญที่ติดตาม : \n"
                    message = message + " " + dataUser[0]['crypto'].replace(",", " ")
        elif word.split(" ")[0] == "remove":
            Helper.FootPrint(lineId, "remove")
            cryptoCurrent = dataUser[0]['crypto']
            crypto = word.split(" ")[1].replace(" ","")
            cryptoArr = cryptoCurrent.replace('(','').replace(')','')[:-1].split(',')
            # if(len(cryptoArr) == 0):
            if "("+crypto+")," in dataUser[0]['crypto']:
                cryptoCurrent = dataUser[0]['crypto'].replace("("+crypto+"),","")
                users.objects.all().filter(line_id=lineId).values().update(crypto=cryptoCurrent)
                message = "ได้ลบเหรียญนี้ '"+ crypto +"' ในบัญชีของคุณแล้ว \n"
                message = message + "เหรียญที่ติดตาม : \n"
                message = message + " " + dataUser[0]['crypto'].replace(",", " ").replace("("+ crypto +")", "").replace("  ", "")
            else:
                message = "ไม่มีเหรียญนี้ '"+ crypto +"' ในบัญชีของคุณแล้ว \n"
                message = message + "เหรียญที่ติดตาม : \n"
                message = message + " " + dataUser[0]['crypto'].replace(",", " ")          
        elif word == "rank":
            Helper.FootPrint(lineId, "rank")
            dataCryptocurrency = cryptocurrency.objects.all().order_by('rank_value')[:5].values()
            
            dataUser = users.objects.all().values()

            count = 0
            message = ""
            for itemValue in dataCryptocurrency :
                count = count + 1
                datavalue = cryptocurrency_value_fifteen.objects.all().filter(crypto=itemValue['name_short']).order_by('-id')[:1].values()
                message = message +" \n"+ str(count) + ". " + str(datavalue[0]['crypto']).upper() +" ราคา $" + Helper.FormatPrice(datavalue[0]['price_usd']) +" ("+str(datavalue[0]['price_usd_percentage'])+ "%)"
            
            dateTime = datetime.now(pytz.timezone('Asia/Bangkok')).strftime('%d-%m-%Y %H:%M:%S')
            message = "เหรียญที่น่าสนใจ "+str(count)+" อันดับ\n วันที่ "+ dateTime +"\n(เปอร์เซ็นต์การเติบโตในช่วง 15 นาที)" + message
        elif word == "popular":
            Helper.FootPrint(lineId, "popular")
            dataCryptocurrency = cryptocurrency.objects.all().values()
            dataCryptocurrencyArr = {}
            for itemValue in dataCryptocurrency :
                dataCryptocurrencyArr[itemValue['name_short']] = 0
            dataUser = users.objects.all().values()
            for itemValue in dataUser :
                cryptoArr = itemValue['crypto'].replace('(','').replace(')','')[:-1]
                cryptoArr = cryptoArr.split(',')
                for itemCrypto in cryptoArr :
                    if(len(itemCrypto) != 0):
                        dataCryptocurrencyArr[str(itemCrypto)] = dataCryptocurrencyArr[str(itemCrypto)] + 1
                        
            dataCryptocurrencyArrSort = sorted(dataCryptocurrencyArr.items(), key=lambda x: x[1], reverse=True)
            message = "เหรียญที่มีผู้ใช้งานติดตามมากที่สุด : "
            count = 0
            del dataCryptocurrencyArrSort[5:]
            for itemCryptocurrencyArrSort in dataCryptocurrencyArrSort :
                count = count + 1
                message = message + "\n" + str(count) + ". " + itemCryptocurrencyArrSort[0] + " จำนวน : " + str(itemCryptocurrencyArrSort[1])
        elif (word.split(" ")[0] == "message"):
            Helper.FootPrint(lineId, "message")
            question = word.split("message ")[1]
            if (question != ""):
                questionInsert = questions()
                questionInsert.line_id = str(lineId)
                questionInsert.question = question
                questionInsert.username = dataUser[0]['username']
                questionInsert.date_time = datetime.now(pytz.timezone('Asia/Bangkok')).strftime('%Y-%m-%d %H:%M:%S')
                questionInsert.status = 0
                questionInsert.save()

                message = ""
                message = message + "ระบบได้ส่งข้อความไปที่ admin เรียบร้อยแล้ว"
                Helper.SendLineNoti('Ufe56503cd2d0b7b0cb0fe4a68a8a11ea', 'มีข้อความส่งถึงคุณ')
            else:
                message = ""
                message = message + "กรุณากรอก คำถาม"
        elif word == "website":
            Helper.FootPrint(lineId, "website")
            title =  '                  Website \n'
            Helper.SendLineLinkNoti(lineId, "https://liff.line.me/1656966432-RdE6Xg0M", "see", title)
            return HttpResponse(200)
        elif word == "dashboard":
            Helper.FootPrint(lineId, "dashboard")
            title =  '                  Dashboard \n'
            Helper.SendLineLinkNoti(lineId, "https://liff.line.me/1656966432-1arg4eEo", "see", title)
            return HttpResponse(200)
        else:
            Helper.FootPrint(lineId, "worng command")
            message = "คำสั่ง : "
            message = message + "\nวีธีการใช้งานคำสั่ง : "
            message = message + "\nพิมคำสั่งในช่องข้อความแล้วกดส่ง"
            for item in command :
                message = message + "\n - " + item
    else:
        Helper.FootPrint(lineId, "not register")
        message = ""
        message = message + "ไม่พบข้อมูลผู้ใช้งาน กรุณาลงทะเบียนที่ website"
        Helper.ReplyLineNoti(jsonBody['events'][0]['replyToken'], message)
        title =  '                  Website \n'
        Helper.SendLineLinkNoti(lineId, "https://liff.line.me/1656966432-4wrBkl0n", "see", title)
        return HttpResponse(200)

    Helper.ReplyLineNoti(jsonBody['events'][0]['replyToken'], message)
    return HttpResponse(200)

#---------------------------------- Webhook ----------------------------------#

#---------------------------------- Graph ----------------------------------#
def GraphDaily(request):
    type = ['open', 'close', 'grow_rate']
    predict = 2 
    dataCryptocurrency = cryptocurrency.objects.all().values()
    dashboard = []
    cryptoString = ""
    for itemCrypto in dataCryptocurrency:
        cryptoString = cryptoString  + "(" + itemCrypto['name_short'] + ") "
    for itemCrypto in dataCryptocurrency :
        crypto = []
        crypto.append([itemCrypto['name_short']])
        for itemType in type :
            dataDaily = cryptocurrency_value_daily.objects.all().filter(crypto=itemCrypto['name_short']).order_by('-id')[:2].values()
            predictDaily = Helper.GetPriceDailyToPredictFromDb(itemCrypto['name_short'], int(predict), itemType)
            predictArr = []
            predictArr.append(dataDaily[1][itemType])
            predictArr.append(dataDaily[0][itemType])
            for itemPredict in predictDaily:
                predictArr.append(itemPredict)
            crypto.append(predictArr)
        dashboard.append(crypto)

    dataDaily = cryptocurrency_value_daily.objects.all().order_by('-id')[:1].values()
    dateTime = dataDaily[0]['date_time']

    dateTimeArrays = []
    dateTimeArrays.append((dateTime  -  timedelta(days=1)).strftime('%d-%m-%Y'))
    dateTimeArrays.append((dateTime).strftime('%d-%m-%Y'))

    for day in range(int(predict)):
        dateTime = dateTime + timedelta(days=1)
        dateTimeArrays.append((dateTime).strftime('%d-%m-%Y'))
    url = '/dashboard/detail/'
    home = '/dashboard/'
    daily = '/graphdaily/'
    return render(request,'dashboardDaily.html',{'dashboard': dashboard,'dateTimeArrays' : dateTimeArrays, 'type' : type , 'cryptoString':cryptoString,'url':url,'home':home, 'daily' : daily})

def DashBoard(request):
    dataCryptocurrency = cryptocurrency.objects.all().values()
    arraysCrypto = []
    limit = 10
    predict = 4
    cryptoString = ""
    for itemCrypto in dataCryptocurrency:
        cryptoString = cryptoString  + "(" + itemCrypto['name_short'] + ") "

    for itemCrypto in dataCryptocurrency:
        prediction = Helper.GetPriceToPredictFromDbGraph(itemCrypto['name_short'], predict, limit)
        arraysCrypto.append(prediction)

    lastest = cryptocurrency_value_fifteen.objects.all().order_by('-id').values()
    lastUpdate = lastest[0]['date_time'].strftime('%H:%M:%S')
    nextUpdate = (lastest[0]['date_time'] + timedelta(minutes=15)).strftime('%H:%M:%S')
    nextRefresh = (lastest[0]['date_time'] + timedelta(minutes=15)).strftime('%Y-%m-%d %H:%M:%S')

    return render(request, 'other/dashboard.html', {'arraysCrypto' : arraysCrypto, 'cryptoString' : cryptoString, 'lastUpdate' : lastUpdate, 'nextUpdate' : nextUpdate, 'nextRefresh' : nextRefresh})

def DashBoardDetail(request):

    crypto  = request.GET.get('crypto')
    dataCryptocurrency = cryptocurrency.objects.all().values()
    arraysCrypto = []
    limit = 10
    predict = 4
    cryptoString = ""
    for itemCrypto in dataCryptocurrency:
        cryptoString = cryptoString  + "(" + itemCrypto['name_short'] + ") "

    prediction = Helper.GetPriceToPredictFromDbGraph(crypto, predict, limit)
    arraysCrypto.append(prediction)

    lastest = cryptocurrency_value_fifteen.objects.all().order_by('-id').values()
    lastUpdate = lastest[0]['date_time'].strftime('%H:%M:%S')
    nextUpdate = (lastest[0]['date_time'] + timedelta(minutes=15)).strftime('%H:%M:%S')
    nextRefresh = (lastest[0]['date_time'] + timedelta(minutes=15)).strftime('%Y-%m-%d %H:%M:%S')
    detail = cryptocurrency_profile.objects.all().filter(crypto=crypto).values()

    profile = {
        'name' : detail[0]['name'],
        'details' : detail[0]['details'],
         'details_technology' : detail[0]['technology_details'],
          'details_background' : detail[0]['background_details'],
          'individuals' : detail[0]['individuals'],
          'organizations' : detail[0]['organizations'],
          'allocated_to_organization_or_founders_percentage' : detail[0]['allocated_to_organization_or_founders_percentage'],
          'allocated_to_investors_percentage' : detail[0]['allocated_to_investors_percentage'],
          'allocated_to_premined_rewards_or_airdrops_percentage' : detail[0]['allocated_to_premined_rewards_or_airdrops_percentage']
    }

    print(cryptoString)
    return render(request, 'other/dashboardDetail.html', {'arraysCrypto' : arraysCrypto, 'cryptoString' : cryptoString, 'lastUpdate' : lastUpdate, 'nextUpdate' : nextUpdate, 'profile' : profile, 'nextRefresh' : nextRefresh})
 
#---------------------------------- Graph ----------------------------------#

#---------------------------------- Other ----------------------------------#

def Help(request):
    commandArr = []
    for item in command:
        subCommandArr = []
        value = str(item).split(":")
        subCommandArr.append(value[0])
        subCommandArr.append(value[1])
        commandArr.append(subCommandArr)
    return render(request,'other/help.html',{'data': commandArr, 'title' : 'คำสั่ง'})

#---------------------------------- Other ----------------------------------#

def Certification(request):
    return render(request,'google54896d84e12d29d5.html')



